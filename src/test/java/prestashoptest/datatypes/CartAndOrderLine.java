package prestashoptest.datatypes;

/**
 * POJO for a product line in a cart or in an order
 */
public class CartAndOrderLine {

    private final String product;
    private final int quantity;
    private final String dimension;
    private final String size;
    private final String color;
    private final String customization;
    private final Float productPrice;
    private final Float totalProductPrice;

    public CartAndOrderLine(final String product,
                            final int quantity,
                            final String dimension,
                            final String size,
                            final String color,
                            final String customization,
                            final Float productPrice,
                            final Float totalProductPrice) {
        super();
        this.product = product;
        this.quantity = quantity;
        this.dimension = dimension;
        this.size = size;
        this.color = color;
        this.customization = customization;
        this.productPrice = productPrice;
        this.totalProductPrice = totalProductPrice;
    }

    @Override
    public String toString() {
        return "[product=" + this.product
                + ", quantity=" + this.quantity
                + ", dimension=" + this.dimension
                + ", size="    + this.size
                + ", color="    + this.color
                + ", customization=" + this.customization
                + ", productPrice=" + this.productPrice
                + ", totalProductPrice=" + this.totalProductPrice + "]";
    }

    public String getProduct() {
        return this.product;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public String getDimension() {
        return this.dimension;
    }

    public String getSize() {
        return this.size;
    }

    public String getColor() {
        return this.color;
    }

    public String getCustomization() {
        return this.customization;
    }

    public Float getProductPrice() {
        return this.productPrice;
    }

    public Float getTotalProductPrice() {
        return this.totalProductPrice;
    }
}